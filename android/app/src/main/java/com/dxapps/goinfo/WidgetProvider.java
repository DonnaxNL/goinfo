package com.dxapps.goinfo;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import android.widget.RemoteViews;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;
import static java.text.DateFormat.*;

public class WidgetProvider extends AppWidgetProvider {
    private static final String REFRESH_ACTION = "REFRESH_ACTION";
    private static final String EXTRA_ID = "EXTRA_ID";
    private static final String EXTRA_SIZE = "EXTRA_SIZE";
    private int mCurrentArrayIndex = 0;
    private boolean isRefreshed = false;
    private AppWidgetManager mAppWidgetManager;
    private int[] mWidgetSize;
    private List<EventDetails> mEventList = new ArrayList<>();

    /**
     * Returns number of cells needed for given size of the widget.
     *
     * @param size Widget size in dp.
     * @return Size in number of cells.
     */
    private static int getCellsForSize(int size) {
        int n = 2;
        while (70 * n - 30 < size) {
            ++n;
        }
        return n - 1;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        mAppWidgetManager = appWidgetManager;

        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId);
        }
    }

    private void updateWidget(Context context, AppWidgetManager appWidgetManager, int widgetId) {
        appWidgetManager.updateAppWidget(widgetId, getRemoteViews(context, widgetId, -1, -1));
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context,
                                          AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        mAppWidgetManager = appWidgetManager;
        Log.d("Flutter", "Changed dimensions");

        // See the dimensions and
        Bundle options = appWidgetManager.getAppWidgetOptions(appWidgetId);

        // Get min width and height.
        int minWidth = options.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH);
        int minHeight = options.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);

        // Obtain appropriate widget and update it.
        appWidgetManager.updateAppWidget(appWidgetId,
                getRemoteViews(context, appWidgetId, minWidth, minHeight));

        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId,
                newOptions);
    }

    /**
     * Determine appropriate view based on width provided.
     *
     * @param minWidth
     * @param minHeight
     * @return
     */
    private RemoteViews getRemoteViews(Context context, int appWidgetId, int minWidth,
                                       int minHeight) {
        // Save widget size
        mWidgetSize = new int[]{minWidth, minHeight};

        // First find out rows and columns based on width provided.
        int rows = minHeight < 0 ? 2 : getCellsForSize(minHeight);
        int columns = minWidth < 0 ? 4 : getCellsForSize(minWidth);

        getAllData(context);
        mCurrentArrayIndex = getLoopIndex(context);

        if (columns == 1) {
            // Only show app icon
            return getSmallView(context);
        } else {
            // Get main remote view.
            return getMainView(context, appWidgetId, columns);
        }
    }

    public RemoteViews getSmallView(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.small_widget);

        // Construct an Intent object.
        Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.dxapps.goinfo");

        // In widget we are not allowing to use intents as usually. We have to use PendingIntent instead of 'startActivity'
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        remoteViews.setOnClickPendingIntent(R.id.imageView, pendingIntent);
        return remoteViews;
    }

    public RemoteViews getMainView(Context context, int widgetId, int columns) {
        Locale locale = Locale.getDefault();
        String date = getDateInstance(SHORT, locale).format(mEventList.get(mCurrentArrayIndex).enddatetime);
        String time = locale.getLanguage().equals("en")
                ? new SimpleDateFormat("hh:mm a", locale).format(mEventList.get(mCurrentArrayIndex).enddatetime)
                : new SimpleDateFormat("HH:mm", locale).format(mEventList.get(mCurrentArrayIndex).enddatetime);
        String countdown;
        long counter;
        Calendar enddate = Calendar.getInstance();
        enddate.setTime(mEventList.get(mCurrentArrayIndex).enddatetime);
        long msDiff = enddate.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();

        if (mEventList.get(mCurrentArrayIndex).id.equals("empty")) {
            countdown = mEventList.get(mCurrentArrayIndex).description;
        } else {
            counter = TimeUnit.MILLISECONDS.toDays(msDiff);
            String counterText = counter == 1 ? "day" : "days";

            if (counter == 0) {
                counter = TimeUnit.MILLISECONDS.toHours(msDiff);
                counterText = counter == 1 ? "hour" : "hours";
            }
            if (columns == 2) {
                countdown = counter + " " + counterText + " left";
                // If less than a hour remaining
                if (counter <= 0) {
                    countdown = "Until " + time;
                }
            } else if (columns == 3) {
                if (counterText.equals("hours")) {
                    counterText = " hrs";
                } else if (counterText.equals("hour")) {
                    counterText = " hr";
                } else {
                    counterText = "d";
                }
                countdown = "Until " + date + ", " + counter + counterText + " left";
                // If less than a hour remaining
                if (counter <= 0) {
                    countdown = "Until " + date + " - " + time;
                }
            } else {
                countdown = "Until " + date + ", " + counter + " " + counterText + " left";
                // If less than a hour remaining
                if (counter <= 0) {
                    countdown = "Until " + date + " - " + time;
                }
            }
        }

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.default_widget);
        remoteViews.setTextViewText(R.id.title, mEventList.get(mCurrentArrayIndex).title);
        remoteViews.setTextViewText(R.id.countdown_text, countdown);

        Picasso.get()
                .load(mEventList.get(mCurrentArrayIndex).image)
                .resize(1024, 512)
                .centerCrop()
                .into(remoteViews, R.id.image, new int[]{widgetId});

        // Main intent
        Intent intent;
        // Construct an Intent object includes deeplink.
        if (!mEventList.get(mCurrentArrayIndex).id.equals("")) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("dxapps://goinfo.com/detailpage?id=" + mEventList.get(mCurrentArrayIndex).id));
        } else {
            intent = context.getPackageManager().getLaunchIntentForPackage("com.dxapps.goinfo");
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.widget_layout, pendingIntent);

        // Refresh
        Intent refreshIntent = new Intent(context, WidgetProvider.class);
        refreshIntent.setAction(REFRESH_ACTION);
        refreshIntent.putExtra(EXTRA_ID, widgetId);
        refreshIntent.putExtra(EXTRA_SIZE, mWidgetSize);
        PendingIntent pendingRefreshIntent = PendingIntent.getBroadcast(context, 0, refreshIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.circulate, pendingRefreshIntent);

        // Logo intent
        Intent logoIntent = context.getPackageManager().getLaunchIntentForPackage("com.dxapps.goinfo");
        PendingIntent pendingLogoIntent = PendingIntent.getActivity(context, 0, logoIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.logo, pendingLogoIntent);

        return remoteViews;
    }

    private void getAllData(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("FlutterSharedPreferences", MODE_PRIVATE);
        String eventJson = prefs.getString("flutter." + "currentEvents", "");

        if (!eventJson.equals("")) {
            mEventList.clear();
            Log.d("Flutter", "android prefs all: " + eventJson);
            try {
                JSONObject jsonObject = new JSONObject(eventJson);
                JSONArray jsonArray = jsonObject.getJSONArray("eventdetails");
                for (int i = 0; i < jsonArray.length() - 1; i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    Calendar enddate = Calendar.getInstance();
                    enddate.setTime(EventDetails.stringToDate(obj.getString("enddatetime")));
                    long msDiff = enddate.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
                    // Check if event from pref has expired
                    if (msDiff > 0) {
                        Log.d("Flutter", "non refresh ms:" + msDiff);
                        mEventList.add(new EventDetails(
                                obj.getString("id"),
                                obj.getString("title"),
                                obj.getString("image"),
                                obj.getString("description"),
                                obj.getString("startdatetime"),
                                obj.getString("enddatetime")));
                    } else {
                        Log.d("Flutter", "refresh ms:" + msDiff +
                                " | skipped id-name: " + i + "-" + obj.getString("title"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (mEventList.isEmpty()) {
                mEventList.add(EventDetails.emptyItem());
            }
        } else {
            mEventList.add(EventDetails.emptyItem());
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (REFRESH_ACTION.equals(intent.getAction())) {
            int widgetId = intent.getIntExtra(EXTRA_ID, 0);
            int[] size = intent.getIntArrayExtra(EXTRA_SIZE);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.updateAppWidget(widgetId,
                    getRemoteViews(context, widgetId, size[0], size[1]));
        }
    }

    private int getLoopIndex(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences("SharedPreferences", MODE_PRIVATE).edit();
        int index = prefs.getInt("loopIndex", 0);
        if (index < mEventList.size()) {
            editor.putInt("loopIndex", index + 1);
            editor.apply();
        } else {
            editor.putInt("loopIndex", 1);
            editor.apply();
            index = 0;
        }
        return index;
    }
}