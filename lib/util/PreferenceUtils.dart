import 'package:goinfo/models/eventdetails.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceUtils {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  static final String _kPrefResearchLastUpdated = "researchLastUpdated";
  static final String _kPrefCurrentEvents = "currentEvents";

  SharedPreferences pref;

  void getInstance() async {
    pref = await SharedPreferences.getInstance();
  }

  Future<String> getResearchLastUpdate() async {
    return pref.getString(_kPrefResearchLastUpdated) ?? '0000';
  }

  Future<bool> setResearchLastUpdate(String value) async {
    return pref.setString(_kPrefResearchLastUpdated, value);
  }

  Future<String> getEvents() async {
    if (pref != null) {
      return pref.getString(_kPrefCurrentEvents);
    }
    return "";
  }

  Future<bool> setEvents(List<EventDetails> events) async {
    if (pref != null) {
      String json = '{"eventdetails":[';
      if (events == null || events.isEmpty) {
        json = "";
      } else {
        for (EventDetails event in events) {
          json += event.toJson().toString() + ",";
        }
        json += ']}';
      }
      return pref.setString(_kPrefCurrentEvents, json);
    }
  }
}