import 'package:flutter/cupertino.dart';

class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  SliverAppBarDelegate(this._container);

  final Container _container;

  @override
  double get minExtent => 108;
  @override
  double get maxExtent => 108;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return _container;
  }

  @override
  bool shouldRebuild(SliverAppBarDelegate oldDelegate) {
    return false;
  }
}