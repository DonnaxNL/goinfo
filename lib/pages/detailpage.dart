import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/layout/bulletpointtext.dart';
import 'package:goinfo/layout/sliverappbardelegate.dart';
import 'package:goinfo/models/eventdetails.dart';
import 'package:goinfo/models/pokemon.dart';
import 'package:goinfo/pages/popout.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

import '../constants.dart';

class DetailPage extends StatefulWidget {
  DetailPage({Key key, this.eventDetails}) : super(key: key);

  final EventDetails eventDetails;

  @override
  _DetailPageState createState() => _DetailPageState();
}

const kExpandedHeight = 200.0;

class _DetailPageState extends State<DetailPage> {
  BuildContext mContext;

  DateFormat dateFormat;
  DateFormat timeFormat;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
    _scrollController = ScrollController()..addListener(() => setState(() {}));
  }

  bool get _hideShadow {
    return _scrollController.hasClients &&
        _scrollController.offset > kExpandedHeight - kToolbarHeight;
  }

  @override
  Widget build(BuildContext context) {
    mContext = context;
    Locale myLocale = Localizations.localeOf(context);
    dateFormat = new DateFormat.yMMMEd(myLocale.languageCode);
    timeFormat = myLocale.languageCode == "en"
        ? new DateFormat.jm("en")
        : new DateFormat.Hm(myLocale.languageCode);

    return Scaffold(
        body: CustomScrollView(
      controller: _scrollController,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Theme.of(context).primaryColor,
          iconTheme: Theme.of(context).primaryIconTheme,
          pinned: true,
          expandedHeight: kExpandedHeight,
          flexibleSpace: FlexibleSpaceBar(
              title: Text(
                widget.eventDetails.title,
                style: TextStyle(
                  color: Colors.white,
                  shadows: _hideShadow
                      ? null
                      : <Shadow>[
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 2.0,
                            color: Color.fromARGB(255, 0, 0, 0),
                          ),
                        ],
                ),
              ),
              background: CachedNetworkImage(
                imageUrl: widget.eventDetails.image,
                fit: BoxFit.cover,
                placeholder: (context, url) =>
                    Image.asset("images/placeholder_image.jpg"),
                errorWidget: (context, url, error) =>
                    Image.asset("images/placeholder_image.jpg"),
              )),
        ),
        SliverPersistentHeader(
          delegate: SliverAppBarDelegate(
            Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(children: <Widget>[
                    Icon(Icons.today, color: Colors.white),
                    SizedBox(width: 8.0),
                    Text(
                        'Starts: ' +
                            dateFormat
                                .format(widget.eventDetails.startdatetime) +
                            " " +
                            timeFormat
                                .format(widget.eventDetails.startdatetime),
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                  ]),
                  SizedBox(height: 6.0),
                  Row(children: <Widget>[
                    Icon(Icons.event, color: Colors.white),
                    SizedBox(width: 8.0),
                    Text(
                        'Ends: ' +
                            dateFormat.format(widget.eventDetails.enddatetime) +
                            " " +
                            timeFormat.format(widget.eventDetails.enddatetime),
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                  ]),
                  SizedBox(height: 6.0),
                  Row(children: <Widget>[
                    Icon(Icons.timelapse, color: Colors.white),
                    SizedBox(width: 8.0),
                    countdown(context, widget.eventDetails),
                  ])
                ],
              ),
            ),
          ),
          pinned: true,
        ),
        SliverList(
          // Use a delegate to build items as they're scrolled on screen.
          delegate: SliverChildListDelegate(
            [_body()],
          ),
        )
      ],
    ));
  }

  Widget _body() {
    return Center(
        child: Column(children: <Widget>[
      // New shinies
      Visibility(
        visible: widget.eventDetails.newshinies != null,
        child: Column(children: <Widget>[
          SizedBox(height: 12.0),
          Text("New shiny Pokémon",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          newShinyTable(),
        ]),
      ),
      // Research Breakthrough
      Visibility(
        visible: widget.eventDetails.researchbreakthrough != null,
        child: Column(children: <Widget>[
          SizedBox(height: 12.0),
          Text("Research Breakthrough Rewards",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          researchBreakthrough(),
        ]),
      ),
      // Featured Pokemon
      Visibility(
        visible: widget.eventDetails.featuredpokemon != null,
        child: Column(children: <Widget>[
          SizedBox(height: 12.0),
          Text("Featured Pokémon",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          featuredPokemon()
        ]),
      ),
      // Divider
      Visibility(
          visible: widget.eventDetails.newshinies != null ||
              widget.eventDetails.featuredpokemon != null ||
              widget.eventDetails.researchbreakthrough != null,
          child: Column(children: <Widget>[SizedBox(height: 8.0), Divider()])),
      // Description
      Container(
          width: double.infinity,
          padding: const EdgeInsets.all(12.0),
          child: Text(widget.eventDetails.description.replaceAll("  ", "\n\n"),
              style: TextStyle(fontSize: 18))),
      // Features
      Visibility(
          visible: widget.eventDetails.features != null,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(12.0),
            child: features(),
          )),
      Visibility(
          visible: widget.eventDetails.bonuses != null,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(12.0),
            child: bonuses(),
          )),
      Visibility(
          visible: widget.eventDetails.officialpost != null,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(16.0),
            child: RaisedButton(
              child: Text('Read official post',
                  style: TextStyle(color: Colors.white)),
              color: Theme.of(context).primaryColor,
              onPressed: () {
                this._openInWebview(widget.eventDetails.officialpost);
              },
            ),
          ))
    ]));
  }

  Widget pokemonTile(Pokemon pokemon, bool shiny, bool withName) {
    String type = "-normal";
    if (shiny) {
      type = "-shiny";
    }
    if (withName) {
      return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
                onTap: () => _showImage(mContext, pokemon, shiny),
                child: Hero(
                    tag: pokemon.mId + type + '-hero-tag',
                    child: CachedNetworkImage(
                      imageUrl:
                          shiny ? pokemon.mImageShiny : pokemon.mImageNormal,
                      width: 75.0,
                      height: 75.0,
                      placeholder: (context, url) =>
                          new CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Image.network(
                          POKEMON_PLACEHOLDER,
                          width: 55,
                          height: 55),
                    )))),
        Text(pokemon.mName.replaceAll("  ", "\n"),
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
      ]);
    } else {
      return Padding(
          padding: const EdgeInsets.all(12.0),
          child: GestureDetector(
              onTap: () => _showImage(mContext, pokemon, shiny),
              child: Hero(
                  tag: pokemon.mId + type + '-hero-tag',
                  child: CachedNetworkImage(
                    imageUrl: pokemon.mImageNormal,
                    width: 55.0,
                    height: 55.0,
                    placeholder: (context, url) =>
                        new CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Image.asset(
                        "images/placeholder_pokemon.jpg",
                        width: 55,
                        height: 55),
                  ))));
    }
  }

  Widget newShinyTable() {
    int columnWidth = 3;
    List<Row> _rows = new List();
    List<Widget> newShinyTable = new List();
    if (widget.eventDetails.newshinies != null) {
      for (var i = 0; i < widget.eventDetails.newshinies.length; i++) {
        newShinyTable.add(streamBuilderPokemon(widget.eventDetails.newshinies, i));
        // Put in rows
        if (i == columnWidth) {
          _rows.add(Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: newShinyTable));
          newShinyTable = new List();
          columnWidth += 4;
        } else if (i == widget.eventDetails.newshinies.length - 1) {
          _rows.add(Row(
              mainAxisAlignment: _rows.length == 0
                  ? MainAxisAlignment.spaceEvenly
                  : MainAxisAlignment.start,
              children: newShinyTable));
        }
      }
    }

    return Container(
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: _rows));
  }

  Widget researchBreakthrough() {
    int columnWidth = 3;
    List<Row> _rows = new List();
    List<Widget> researchBreakthrough = new List();
    if (widget.eventDetails.researchbreakthrough != null) {
      for (var i = 0;
          i < widget.eventDetails.researchbreakthrough.length;
          i++) {
        researchBreakthrough.add(streamBuilderPokemon(widget.eventDetails.researchbreakthrough, i));
        if (i == columnWidth) {
          _rows.add(Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: researchBreakthrough));
          researchBreakthrough = new List();
          columnWidth += 4;
        } else if (i == widget.eventDetails.researchbreakthrough.length - 1) {
          _rows.add(Row(
              mainAxisAlignment: _rows.length == 0
                  ? MainAxisAlignment.spaceEvenly
                  : MainAxisAlignment.start,
              children: researchBreakthrough));
        }
      }
    }

    return Container(
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: _rows));
  }

  Widget featuredPokemon() {
    int columnWidth = 4;
    List<Row> _rows = [];
    List<Widget> featuredTable = [];
    if (widget.eventDetails.featuredpokemon != null) {
      for (var i = 0; i < widget.eventDetails.featuredpokemon.length; i++) {
        featuredTable.add(streamBuilderPokemon(widget.eventDetails.featuredpokemon, i));
        if (i == columnWidth) {
          _rows.add(Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: featuredTable));
          featuredTable = [];
          columnWidth += 5;
        } else if (i == widget.eventDetails.featuredpokemon.length - 1) {
          _rows.add(Row(
              mainAxisAlignment: _rows.length == 0
                  ? MainAxisAlignment.spaceEvenly
                  : MainAxisAlignment.start,
              children: featuredTable));
        }
      }
    }

    return Container(
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: _rows));
  }

  Widget features() {
    List<Widget> features = new List();
    List<dynamic> featureList = widget.eventDetails.features;

    features.add(Text("Features",
        textAlign: TextAlign.start,
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)));
    features.add(SizedBox(height: 8.0));
    if (featureList != null) {
      for (var i = 0; i < featureList.length; i++) {
        features.add(BulletPointText(featureList[i].replaceAll("  ", "\n"),
            style: TextStyle(fontSize: 16)));
        features.add(SizedBox(height: 8.0));
      }
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start, children: features);
  }

  Widget bonuses() {
    List<Widget> bonuses = new List();
    List<dynamic> bonusList = widget.eventDetails.bonuses;

    bonuses.add(Text("Bonuses",
        textAlign: TextAlign.start,
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)));
    bonuses.add(SizedBox(height: 8.0));
    if (bonusList != null) {
      for (var i = 0; i < bonusList.length; i++) {
        bonuses
            .add(BulletPointText(bonusList[i], style: TextStyle(fontSize: 16)));
        bonuses.add(SizedBox(height: 8.0));
      }
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start, children: bonuses);
  }

  Widget countdown(BuildContext context, EventDetails event) {
    return StreamBuilder(
        stream: Stream.periodic(Duration(seconds: 1), (i) => i),
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          DateTime now = DateTime.now();

          if (widget.eventDetails.startdatetime.isBefore(now) &&
              widget.eventDetails.enddatetime.isAfter(now)) {
            // Current
            var difference = event.enddatetime.difference(now);
            final days = difference.inDays;
            final hours = difference.inHours - difference.inDays * 24;
            final minutes = difference.inMinutes - difference.inHours * 60;
            final seconds = difference.inSeconds - difference.inMinutes * 60;

            final formattedRemaining =
                '$days days $hours hrs $minutes min $seconds sec';

            return Text('Time left: ' + formattedRemaining,
                style: TextStyle(color: Colors.white, fontSize: 18));
          } else if (widget.eventDetails.startdatetime.isAfter(now)) {
            // Future
            var difference = event.startdatetime.difference(now);
            final days = difference.inDays;
            final hours = difference.inHours - difference.inDays * 24;
            final minutes = difference.inMinutes - difference.inHours * 60;
            final seconds = difference.inSeconds - difference.inMinutes * 60;

            final formattedStarting =
                '$days days $hours hrs $minutes min $seconds sec';
            return Text('Starting in: ' + formattedStarting,
                style: TextStyle(color: Colors.white, fontSize: 18));
          } else {
            return Text('Event ended',
                style: TextStyle(color: Colors.white, fontSize: 18));
          }
        });
  }

  Future<Null> _openInWebview(String url) async {
    if (await url_launcher.canLaunch(url)) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (ctx) => WebviewScaffold(
            initialChild: Center(child: CircularProgressIndicator()),
            url: url,
            appBar: AppBar(
                iconTheme: Theme.of(context).primaryIconTheme,
                title: Text('Pokémon GO Website')),
          ),
        ),
      );
    } else {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('URL $url can not be launched.'),
        ),
      );
    }
  }

  void _showImage(BuildContext context, Pokemon pokemon, bool shiny) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>
            Popout(pokemon: pokemon, isShiny: shiny)));
  }

  streamBuilderPokemon(List<dynamic> stream, int i) {
    return StreamBuilder<DocumentSnapshot>(
        stream: stream[i].snapshots(),
        builder: (_, snapshot) {
          if (!snapshot.hasData) {
            Pokemon pokemon = new Pokemon('missingNo', 'Pokémon not found',
                '???', POKEMON_PLACEHOLDER, null, false, null);
            //return pokemonTile(pokemon, false, true);
            return new CircularProgressIndicator();
          } else {
            DocumentSnapshot data = snapshot.data;
            //Safe pit
            String name =
            snapshot.data['name'] != null ? snapshot.data['name'] : "";
            String researchCp = getSnapshot(data, 'research-cp');
            String imageNormal = snapshot.data['image-normal'] != null
                ? snapshot.data['image-normal']
                : POKEMON_PLACEHOLDER;
            String imageShiny = snapshot.data['image-shiny'] != null
                ? snapshot.data['image-shiny']
                : POKEMON_PLACEHOLDER;
            bool shiny = snapshot.data['shiny-available'] != null
                ? snapshot.data['shiny-available']
                : false;
            String regional = getSnapshot(data, 'regional');

            Pokemon pokemon = new Pokemon(
                stream[i].id,
                name,
                researchCp,
                imageNormal,
                imageShiny,
                shiny,
                regional);
            return pokemonTile(pokemon, false, true);
          }
        });
  }

  getSnapshot(DocumentSnapshot snapshot, String key) {
    try {
      return snapshot.get(key);
    } catch (error) {
      return null;
    }
  }
}
