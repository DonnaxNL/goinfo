import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants.dart';

List<Widget> about() {
  _launchURL(String toMailId, String cc, String subject, String body) async {
    var url = 'mailto:$toMailId?cc=$cc&subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  return <Widget>[
    new ListTile(
      leading: Image.asset("images/app_icon.png", width: 40, height: 40),
      title: new Text(APP_DESCRIPTION),
      subtitle: new Text(APP_VERSION),
    ),
    new Divider(),
    new ListTile(
      leading: new Icon(Icons.shop),
      title: new Text('Rate on Google Play'),
      onTap: () {
        launch(STORE_ANDROID_LINK);
      },
    ),
    new ListTile(
      leading: new Icon(Icons.email),
      title: new Text('Feedback by E-mail'),
      onTap: () {
        _launchURL("dxappsmail@gmail.com", "donavanlb@gmail.com", EMAIL_SUBJECT,
            EMAIL_BODY);
      },
    ),
    new ListTile(
      leading: new Icon(Icons.share),
      title: new Text('Share this app'),
      onTap: () {
        //Share.share(SHARE_TEXT);
      },
    ),
    new ListTile(
      leading: new Icon(Icons.monetization_on),
      title: new Text('Donate to developer'),
      onTap: () {
        launch(DONATE_LINK);
      },
    ),
    new Divider(),
    Padding(
      padding: EdgeInsets.all(4.0),
      child:new Text(
        APP_DEVELOPMENT,
        textAlign: TextAlign.end,
        style: new TextStyle(color: Colors.black54, fontSize: 14.0),
      ),
    )
  ];
}
