import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EventDetails {
  String id;
  bool hidden;
  String title;
  String description;
  String image;
  String eventtype;
  String rawEventtypecolor;
  Color eventtypecolor;
  DateTime startdatetime;
  DateTime enddatetime;
  List<dynamic> newshinies = new List();
  List<dynamic> researchbreakthrough = new List();
  List<dynamic> featuredpokemon = new List();
  List<dynamic> features = new List();
  List<dynamic> bonuses = new List();
  String officialpost;

  // Reference to this record as a firestore document.
  final DocumentReference firestoreDocReference;

  EventDetails(this.title,this.image,this.description,this.eventtype,this.eventtypecolor,this.startdatetime,this.enddatetime, this.firestoreDocReference);

  EventDetails.fromMap(String documentID, Map<String, dynamic> map, {@required this.firestoreDocReference}) {
    //print(documentID);
    assert(map['title'] != null);
    assert(map['description'] != null);
    assert(map['image'] != null);
    assert(map['event-type'] != null);
    assert(map['startdatetime'] != null);
    assert(map['enddatetime'] != null);

    id = documentID;
    hidden = map['hide'];
    title = map['title'];
    description = map['description'];
    image = map['image'];
    eventtype = map['event-type']['title'];
    rawEventtypecolor = map['event-type']['color'];
    eventtypecolor = getColor(map['event-type']['color']);
    startdatetime = parseTime(map['startdatetime']);
    enddatetime = parseTime(map['enddatetime']);
    newshinies = map['new-shinies'];

    researchbreakthrough = map['research-breakthrough'];
    featuredpokemon = map['featured-pokemon'];
    features = map['features'];
    bonuses = map['bonuses'];
    if (map['localtime'] != null) {
      startdatetime = new DateTime(startdatetime.year, startdatetime.month, startdatetime.day, map['localtime']['start'], map['localtime']['minutes']);
      enddatetime = new DateTime(enddatetime.year, enddatetime.month, enddatetime.day, map['localtime']['end'], map['localtime']['minutes']);
    }
    officialpost = map['official-post'];
  }

  EventDetails.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.id, snapshot.data(), firestoreDocReference: snapshot.reference);

  static Color getColor(String color) {
    switch(color) {
      case "blue":
        return Colors.blue; // Community Day
      case "cyan":
        return Colors.cyan; // Regular Event
      case "red":
        return Colors.red; // Live Event
      case "green":
        return Colors.green; // Raid Hour/Day
      case "purple":
        return Colors.purple; // Challenge
      case "brown":
        return Colors.brown; // Research
      default:
        return Colors.cyan;
    }
  }

  static DateTime parseTime(dynamic date) {
    return Platform.isAndroid ? (date as Timestamp).toDate() : date as DateTime;
  }

  Map<String, dynamic> toJson() => _itemToJson(this);

  Map<String, dynamic> _itemToJson(EventDetails instance) {
    return <String, dynamic>{
      '"id"': '"' + instance.id + '"',
      '"title"': '"' + instance.title + '"',
      '"description"': '"' + instance.description + '"',
      '"image"': '"' + instance.image + '"',
      '"eventtype"': '"' + instance.eventtype + '"',
      '"eventtypecolor"': '"' + instance.rawEventtypecolor + '"',
      '"startdatetime"': '"' + instance.startdatetime.toString() + '"',
      '"enddatetime"': '"' + instance.enddatetime.toString() + '"',
    };
  }
}