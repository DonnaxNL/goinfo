import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/models/pokemon.dart';

class ResearchDetails {
  String quest;
  bool hide;
  bool special;
  Map<dynamic, dynamic> rawRewards;
  List<DocumentReference> rewardReferences = new List();

  // Reference to this record as a firestore document.
  final DocumentReference firestoreDocReference;

  ResearchDetails.fromMap(Map<String, dynamic> map, {@required this.firestoreDocReference}) {
    assert(map['quest'] != null);
    assert(map['rewards'] != null);

    quest = map['quest'];
    hide = map['hide'];
    rawRewards = map['rewards'];

    if (rawRewards != null) {
      for (var i = 1; i <= rawRewards.length; i++) {
        DocumentReference current = rawRewards['pokemon-' + i.toString()];
        rewardReferences.add(current);
      }
    }
  }

  ResearchDetails.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), firestoreDocReference: snapshot.reference);
}