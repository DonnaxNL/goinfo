import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/models/pokemon.dart';
import 'package:goinfo/models/researchdetails.dart';

import '../constants.dart';

class ResearchTab extends StatefulWidget {
  ResearchTab({Key key}) : super(key: key);

  @override
  _ResearchState createState() => _ResearchState();
}

class _ResearchState extends State<ResearchTab> {
  Stream<QuerySnapshot> researchStream;

  @override
  void initState() {
    super.initState();
    researchStream = FirebaseFirestore.instance
        .collection('research')
        .where("hide", isEqualTo: false)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: researchStream,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              final List<ResearchDetails> records = snapshot.data.docs
                  .map((snapshot) => ResearchDetails.fromSnapshot(snapshot))
                  .toList();
              return ListView(
                  padding: EdgeInsets.all(8.0),
                  children:
                      records.map((record) => _buildListItem(record)).toList());
            }
          },
        ),
      ),
    );
  }

  // Build a list item corresponding to a research.
  Widget _buildListItem(ResearchDetails research) {
    return ExpansionTile(
      key: PageStorageKey<ResearchDetails>(research),
      title: Text(research.quest),
      children: _buildList(research.rewardReferences),
    );
  }

  List<Widget> _buildList(List<DocumentReference> research) {
    List<Widget> list = new List();
    if (research != null) {
      for (DocumentReference doc in research) {
        list.add(StreamBuilder<DocumentSnapshot>(
            stream: doc.snapshots(),
            builder: (_, snapshot) {
              if (!snapshot.hasData) {
                Pokemon reward = new Pokemon("missingNo", 'Pokémon not found',
                    '???', POKEMON_PLACEHOLDER, null, false, null);
                return _buildPokemonItem(reward);
              } else {
                //Safe pit
                String name =
                    snapshot.data['name'] != null ? snapshot.data['name'] : "";
                String cp = snapshot.data['research-cp'] != null
                    ? snapshot.data['research-cp']
                    : "";
                String image = snapshot.data['image-normal'] != null
                    ? snapshot.data['image-normal']
                    : POKEMON_PLACEHOLDER;
                bool shiny = snapshot.data['shiny-available'] != null
                    ? snapshot.data['shiny-available']
                    : false;

                Pokemon reward = new Pokemon(
                    doc.id, name, cp, image, null, shiny, null);
                return _buildPokemonItem(reward);
              }
            }));
      }
    }
    return list;
  }

  // Build a list item corresponding to a research.
  Widget _buildPokemonItem(Pokemon pokemon) {
    return ListTile(
      leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CachedNetworkImage(
            imageUrl: pokemon.mImageNormal,
            placeholder: (context, url) => new CircularProgressIndicator(),
            errorWidget: (context, url, error) =>
                Image.asset("images/placeholder_pokemon.jpg"),
          )),
      title: Text(pokemon.mName),
      subtitle: Text("cp " + pokemon.mResearchCp),
      trailing: Visibility(
          visible: pokemon.isShiny,
          child: Image.asset("images/shiny_icon.png", width: 24, height: 24)),
    );
  }
}
