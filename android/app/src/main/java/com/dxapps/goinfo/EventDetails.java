package com.dxapps.goinfo;

import android.graphics.Color;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class EventDetails {
    String id;
    boolean hidden;
    String title;
    String description;
    String image;
    String eventtype;
    int eventtypecolor;
    Date startdatetime;
    Date enddatetime;

    EventDetails(String id, String title, String image, String description, String startdate, String enddate) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.description = description;
        //this.eventtype = eventtype;
        //this.eventtypecolor = getColor(eventtypecolor);
        this.startdatetime = stringToDate(startdate);
        this.enddatetime = stringToDate(enddate);
    }

    static int getColor(String color) {
        switch (color) {
            case "blue":
                return Color.BLUE; // Community Day
            case "cyan":
                return Color.CYAN; // Regular Event
            case "red":
                return Color.RED; // Live Event
            case "green":
                return Color.GREEN; // Raid Hour/Day
            case "purple":
                return Color.rgb(128, 0, 128); // Challenge
            case "brown":
                return Color.rgb(165, 42, 42); // Research
            default:
                return Color.CYAN;
        }
    }

    public static Date stringToDate(String aDate) {
        String format = "yyyy-MM-dd HH:mm:ss";
        if (aDate == null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(format, Locale.getDefault());
        return simpledateformat.parse(aDate, pos);
    }

    public static EventDetails emptyItem() {
        return new EventDetails("empty",
                "No events in progress",
                "https://assets.pokemon.com/assets/cms2/img/video-games/_tiles/pokemon-go/07102019/01/pokemon-go-169.jpg",
                "Please open the app to refresh event data.",
                "2100-01-01 00:00:00.000",
                "2100-01-01 00:00:00.000");
    }

    @Override
    public String toString() {
        return id + "," + title + "," + image;
    }
}
