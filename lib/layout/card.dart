import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/constants.dart';
import 'package:goinfo/models/eventdetails.dart';
import 'package:intl/intl.dart';

class EventCard extends StatefulWidget {
  EventCard({Key key, this.eventDetails, this.eventType}) : super(key: key);

  final EventDetails eventDetails;
  final EventType eventType;

  @override
  _CardState createState() => _CardState();
}

class _CardState extends State<EventCard> {
  DateFormat dateFormat;
  DateFormat timeFormat;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Locale locale = Localizations.localeOf(context);
    if (locale.countryCode != null) {
      dateFormat =
          DateFormat.yMd(locale.languageCode + "_" + locale.countryCode);
    } else {
      dateFormat = DateFormat('dd-MM-yyyy');
    }
    timeFormat = locale.languageCode == "en"
        ? new DateFormat.jm("en")
        : new DateFormat.Hm(locale.languageCode);

    return Card(
      color: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0))),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 180.0,
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: CachedNetworkImage(
                    imageUrl: widget.eventDetails.image,
                    imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.fill),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16.0)),
                    )),
                    placeholder: (context, url) => Container(
                        decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("images/placeholder_image.jpg"),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16.0)),
                    )),
                    errorWidget: (context, url, error) => Container(
                        decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("images/placeholder_image.jpg"),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16.0)),
                    )),
                  ),
                ),
                Positioned(
                  bottom: 24.0,
                  left: 16.0,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.eventDetails.title,
                      style: Theme.of(context).textTheme.headline6.copyWith(
                        color: Colors.white,
                        shadows: <Shadow>[
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 2.0,
                            color: Color.fromARGB(255, 0, 0, 0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 8.0,
                  left: 16.0,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.eventType == EventType.CURRENT
                          ? 'Started: ' +
                              getDateTimeFormat(
                                  widget.eventDetails.startdatetime)
                          : 'Starting: ' +
                              getDateTimeFormat(
                                  widget.eventDetails.startdatetime),
                      style: TextStyle(color: Colors.white, shadows: <Shadow>[
                        Shadow(
                          offset: Offset(1.0, 1.0),
                          blurRadius: 2.0,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ]),
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.eventDetails.eventtype != "",
                  child: Positioned(
                    top: 0.0,
                    left: 0.0,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: widget.eventDetails.eventtypecolor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(16.0),
                            bottomRight: Radius.circular(16.0)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          widget.eventDetails.eventtype,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          ButtonTheme(
            child: ButtonBar(
              alignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                countdown(widget.eventDetails, widget.eventType)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget countdown(EventDetails event, EventType type) {
    return StreamBuilder(
        stream:
            Stream.periodic(Duration(seconds: 1), (i) => i).asBroadcastStream(),
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          DateTime now = DateTime.now();
          if (type == EventType.CURRENT) {
            var difference = event.enddatetime.difference(now);
            final days = difference.inDays;
            final hours = difference.inHours - difference.inDays * 24;
            final minutes = difference.inMinutes - difference.inHours * 60;
            final seconds = difference.inSeconds - difference.inMinutes * 60;

            final formattedRemaining =
                '$days days $hours hours $minutes minutes $seconds seconds';

            return Text('Time remaining:  \n' + formattedRemaining,
                textAlign: TextAlign.center, style: TextStyle(fontSize: 16));
          } else {
            var difference = event.startdatetime.difference(now);
            final days = difference.inDays;
            final hours = difference.inHours - difference.inDays * 24;
            final minutes = difference.inMinutes - difference.inHours * 60;
            final seconds = difference.inSeconds - difference.inMinutes * 60;

            final formattedStarting =
                '$days days $hours hours $minutes minutes $seconds seconds';
            return Text('Starting in:  \n' + formattedStarting,
                textAlign: TextAlign.center, style: TextStyle(fontSize: 16));
          }
        });
  }

  String getDateTimeFormat(DateTime dateTime) {
    return dateFormat.format(dateTime) + " - " + timeFormat.format(dateTime);
  }
}
