import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/layout/card.dart';
import 'package:goinfo/models/eventdetails.dart';
import 'package:goinfo/pages/detailpage.dart';
import 'package:intl/intl.dart';
import '../constants.dart';
import '../main.dart';

class EventTab extends StatefulWidget {
  EventTab({Key key}) : super(key: key);

  @override
  _EventState createState() => _EventState();
}

class _EventState extends State<EventTab> {
  int _currentTabIndex = 1;
  DateFormat dateFormat;
  DateFormat dateTimeFormat;

  Stream<QuerySnapshot> eventStream;
  List<EventDetails> records;
  @override
  void initState() {
    super.initState();
    eventStream = FirebaseFirestore.instance
        .collection('events')
        .orderBy("startdatetime", descending: false)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    Locale myLocale = Localizations.localeOf(context);
    if (myLocale.countryCode != null) {
      dateFormat =
          DateFormat.yMd(myLocale.languageCode + "_" + myLocale.countryCode);
    } else {
      dateFormat = DateFormat('dd-MM-yyyy');
    }

    final _kTabPages = <Widget>[
      // Future
      Center(
        child: new StreamBuilder<QuerySnapshot>(
          stream: eventStream,
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              records = snapshot.data.docs
                  .map((snapshot) => EventDetails.fromSnapshot(snapshot))
                  .toList();
              return new ListView(
                  padding: EdgeInsets.all(12.0),
                  children: _buildList(context, records, EventType.FUTURE));
            }
          },
        ),
      ),
      // Current
      Center(
        child: new StreamBuilder<QuerySnapshot>(
          stream: eventStream,
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              records = snapshot.data.docs
                  .map((snapshot) => EventDetails.fromSnapshot(snapshot))
                  .toList();
              return new ListView(
                  padding: EdgeInsets.all(12.0),
                  children: _buildList(context, records, EventType.CURRENT));
            }
          },
        ),
      ),
      // Past
      Center(
        child: new StreamBuilder<QuerySnapshot>(
            stream: eventStream,
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) {
                return CircularProgressIndicator();
              } else {
                records = snapshot.data.docs
                    .map((snapshot) => EventDetails.fromSnapshot(snapshot))
                    .toList();
                return new ListView(
                    padding: EdgeInsets.all(2.0),
                    children: _buildList(context, records, EventType.PAST));
              }
            }),
      ),
    ];

    final _kBottomNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(icon: Icon(Icons.event), label: 'Upcoming'),
      BottomNavigationBarItem(
          icon: Icon(Icons.event_available), label: 'Current'),
      BottomNavigationBarItem(
          icon: Icon(Icons.event_busy), label: 'Past'),
    ];
    assert(_kTabPages.length == _kBottomNavBarItems.length);

    return Scaffold(
      body: _kTabPages[_currentTabIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: _kBottomNavBarItems,
        currentIndex: _currentTabIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          setState(() {
            _currentTabIndex = index;
          });
        },
      ),
    );
  }

  // Build a list item corresponding to an event.
  Widget _buildListItem(
      BuildContext context, EventDetails event, EventType type) {
    return new GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    DetailPage(eventDetails: event),
              ));
        },
        child: type == EventType.PAST
            ? ListTile(
//          leading: CircleAvatar(
//            radius: 30.0,
//            backgroundImage: NetworkImage(event.image),
//            backgroundColor: Colors.transparent,
//          ),
                title: Text(event.title),
                subtitle: Text(
                  dateFormat.format(event.startdatetime) +
                      " - " +
                      dateFormat.format(event.enddatetime),
                ),
                trailing: Text(event.eventtype),
              )
            : EventCard(eventDetails: event, eventType: type));
  }

  List<Widget> _buildList(
      BuildContext context, List<EventDetails> events, EventType type) {
    String text;
    bool firstTimeLongEventCheck = false;
    List<EventDetails> reverseList = [];
    List<EventDetails> prefList = [];
    List<Widget> allRecords = [];
    List<Widget> tempList = [];
    DateTime now = DateTime.now();

    // Reverse list
    reverseList = events.reversed.toList();

    for (EventDetails event in reverseList) {
      if (event.hidden == null || event.hidden == false) {
        switch (type) {
          case EventType.FUTURE:
            if (event.startdatetime.isAfter(now)) {
              tempList.add(_buildListItem(context, event, type));
            }
            allRecords = tempList.reversed.toList();
            break;
          case EventType.CURRENT:
            if (event.startdatetime.isBefore(now) &&
                event.enddatetime.isAfter(now)) {
              var difference =
                  event.enddatetime.difference(event.startdatetime).inDays;
              // Long period events; longer than 21 days
              if (difference > 21) {
                if (!firstTimeLongEventCheck) {
                  firstTimeLongEventCheck = true;
                  if (allRecords.isNotEmpty) {
                    allRecords.add(SizedBox(height: 8.0));
                    allRecords.add(new Divider());
                  }
                  allRecords.add(SizedBox(height: 6.0));
                  allRecords.add(new Text("Long-term events",
                      textAlign: TextAlign.start,
                      style: TextStyle(fontSize: 18)));
                }
                allRecords.add(_buildListItem(context, event, EventType.PAST));
                prefList.add(event);
              } else {
                allRecords.add(_buildListItem(context, event, type));
                prefList.add(event);
              }
              ThisApp.mSharedPrefs.setEvents(prefList);
            }
            break;
          case EventType.PAST:
            if (event.enddatetime.isBefore(now)) {
              allRecords.add(_buildListItem(context, event, type));
            }
            break;
          default:
            break;
        }
      }
    }

    if (allRecords.isEmpty) {
      switch (type) {
        case EventType.FUTURE:
          text = 'There are no upcoming events planned right now.';
          break;
        case EventType.CURRENT:
          ThisApp.mSharedPrefs.setEvents(prefList);
          _currentTabIndex = 0;
          text = 'No events in progress.';
          break;
        case EventType.PAST:
          text = 'No past events found.';
          break;
        default:
          text = 'No events.';
          break;
      }

      allRecords.add(Center(
          child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(12.0),
              child: Text(text,
                  style:
                      TextStyle(fontSize: 16, fontWeight: FontWeight.bold)))));
    }

    return allRecords;
  }
}
