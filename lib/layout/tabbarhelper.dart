import 'package:flutter/material.dart';

class TabBarHelper extends StatelessWidget {
  final List<Widget> tabs;
  final List<Widget> tabPages;

  const TabBarHelper({
    Key key,
    @required this.tabs,
    @required this.tabPages,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabPages.length,
      child: Scaffold(
        appBar: _ColoredTabBar(
          color: Theme.of(context).primaryColor,
          tabBar: TabBar(
              labelColor: Colors.white,
              indicatorColor: Colors.white,
              tabs: tabs,
          ),
        ),
        body: TabBarView(
          children: makeAlwaysAliveWidgetList(tabPages),
        ),
      ),
    );
  }

  List<Widget> makeAlwaysAliveWidgetList(List<Widget> widgets) {
    List<Widget> alwaysAliveWidgets = new List();

    for (var i = 0; i < widgets.length; i++) {
      alwaysAliveWidgets.add(_AlwaysAliveWidget(child: widgets[i]));
    }
    return alwaysAliveWidgets;
  }
}

// This widget is always kept alive, so that when tab is switched back, its
// child's state is still preserved.
class _AlwaysAliveWidget extends StatefulWidget {
  final Widget child;

  const _AlwaysAliveWidget({Key key, @required this.child}) : super(key: key);

  @override
  _AlwaysAliveWidgetState createState() => _AlwaysAliveWidgetState();
}

class _AlwaysAliveWidgetState extends State<_AlwaysAliveWidget>
    with AutomaticKeepAliveClientMixin<_AlwaysAliveWidget> {
  @override
  Widget build(BuildContext context) {
    super.build(context); // This build method is annotated "@mustCallSuper".
    return this.widget.child;
  }

  @override
  bool get wantKeepAlive => true;
}

class _ColoredTabBar extends Container implements PreferredSizeWidget {
  final Color color;
  final TabBar tabBar;

  _ColoredTabBar({Key key, @required this.color, @required this.tabBar})
      : super(key: key);

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) => Material(
    elevation: 4.0,
    color: color,
    child: tabBar,
  );
}
