import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:backdrop/backdrop.dart';
import 'package:goinfo/constants.dart';
import 'package:goinfo/layout/tabbarhelper.dart';
import 'eventtab.dart';
import 'raidtab.dart';
import 'researchtab.dart';
import 'about.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final _mTabs = <Tab>[
      Tab(text: 'Events'),
      Tab(text: 'Field Research'),
      //Tab(text: '5* Raid Boss')
    ];

    final _mTabPages = <Widget>[
      EventTab(),
      ResearchTab(),
      //RaidTab()
    ];

    return BackdropScaffold(
        headerHeight: 440,
        appBar: BackdropAppBar(
          backgroundColor: Color(0xFF25CCAA),
          title: Text(APP_NAME),
        ),
        frontLayer: Builder(
          builder: (BuildContext context) => TabBarHelper(
            tabs: _mTabs,
            tabPages: _mTabPages,
          ),
        ),
        backLayer: ListView(
            children: about()));
    // This trailing comma makes auto-formatting nicer for build methods
  }
}
