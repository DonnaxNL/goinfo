import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Pokemon {
  String mId;
  String mName;
  String mResearchCp;
  String mImageNormal;
  String mImageShiny;
  String mRegional;
  bool isShiny;

  // Reference to this record as a firestore document.
  DocumentReference firestoreDocReference;

  Pokemon(this.mId, this.mName, this.mResearchCp, this.mImageNormal, this.mImageShiny, this.isShiny, this.mRegional);

  Pokemon.fromMap(Map<String, dynamic> map, {@required this.firestoreDocReference}) {
  }

  Pokemon.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), firestoreDocReference: snapshot.reference);
}