// *Note*: when APP_VERSION is changed, remember to also update pubspec.yaml.
const APP_VERSION = '0.4.2';
const APP_NAME = 'GO Info';
const APP_DESCRIPTION = 'Keep up with all Pokémon GO event details and more!';
const APP_DEVELOPMENT = 'Designed and developed by Donavan A. (DxApps)';
const STORE_ANDROID_LINK = 'https://play.google.com/store/apps/details?id=com.dxapps.goinfo';
const STORE_IOS_LINK = '';
const EMAIL_SUBJECT = 'GO Info Feedback';
const EMAIL_BODY = 'App version: ' + APP_VERSION + "\n\n\n";
const SHARE_TEXT = 'Check out this app! GO Info - ' + STORE_ANDROID_LINK;
const POKEMON_PLACEHOLDER = 'https://images.weserv.nl/?trim=20&url=raw.githubusercontent.com/ZeChrales/PogoAssets/master/pokemon_icons/pokemon_icon_000.png';
const DONATE_LINK = 'https://paypal.me/donavanasmawidjaja';

enum EventType { FUTURE, CURRENT, PAST }