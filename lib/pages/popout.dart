import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/models/pokemon.dart';

class Popout extends StatefulWidget {
  Popout({Key key, this.pokemon, this.isShiny}) : super(key: key);

  final Pokemon pokemon;
  final bool isShiny;

  @override
  _PopoutState createState() => _PopoutState();
}

class _PopoutState extends State<Popout> {
  String type = "-normal";

  @override
  Widget build(BuildContext context) {
    if (widget.isShiny) {
      type = "-shiny";
    }

    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
            title: Text(widget.pokemon.mName
                    .replaceAll("  ", " ")
                    .replaceAll(" (as evolution)", "")),
            backgroundColor: Colors.transparent),
        body: Center(
                child: Hero(
                    tag: widget.pokemon.mId + type + '-hero-tag',
                    child: CachedNetworkImage(
                      imageUrl: widget.isShiny
                          ? widget.pokemon.mImageShiny
                          : widget.pokemon.mImageNormal,
                      placeholder: (context, url) =>
                          new CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          Image.asset("images/placeholder_pokemon.jpg"),
                    ))));
  }
}
