import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:goinfo/constants.dart';
import 'package:goinfo/models/eventdetails.dart';
import 'package:goinfo/pages/detailpage.dart';
import 'package:goinfo/pages/homepage.dart';
import 'package:goinfo/util/PreferenceUtils.dart';
import 'package:uni_links/uni_links.dart';

void main() async {
  bool isInDebugMode = false;

  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Crashlytics.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  Future<Null> initUniLinks() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      String initialLink = await getInitialLink();
      // Parse the link and warn the user, if it is not correct,
      // but keep in mind it could be `null`.
      if (initialLink != null) {
        print("linking: " + initialLink);
        List split = initialLink.split('=');
        String documentId = split[1];
        DocumentReference documentReference =
        FirebaseFirestore.instance.collection("events").doc(documentId);
        documentReference.get().then((datasnapshot) {
          if (datasnapshot.exists) {
            EventDetails event = EventDetails.fromSnapshot(datasnapshot);
            runApp(ThisApp(widget: DetailPage(eventDetails: event)));
          } else {
            print("No event found");
          }
        });
      } else {
        runApp(ThisApp(widget: HomePage()));
      }
    } on PlatformException {
      // Handle exception by warning the user their action did not succeed
      // return?
      runApp(ThisApp(widget: HomePage()));
    }
  }
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  initUniLinks();
  //runApp(ThisApp(widget: HomePage()));

  // runZoned<Future<Null>>(() async {
  //   WidgetsFlutterBinding.ensureInitialized();
  //   initUniLinks();
  // }, onError: (error, stackTrace) async {
  //   // Whenever an error occurs, call the `reportCrash` function. This will send
  //   // Dart errors to our dev console or Crashlytics depending on the environment.
  //   //await FlutterCrashlytics().reportCrash(error, stackTrace, forceCrash: true);
  // });
}

class ThisApp extends StatelessWidget {
  static PreferenceUtils mSharedPrefs;

  ThisApp({Key key, this.widget}) : super(key: key);

  final Widget widget;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final Future<FirebaseApp> _initialization = Firebase.initializeApp();
    mSharedPrefs = new PreferenceUtils();
    mSharedPrefs.getInstance();

    // return FutureBuilder(
    //   // Initialize FlutterFire:
    //   future: _initialization,
    //   builder: (context, snapshot) {
        // // Check for errors
        // if (snapshot.hasError) {
        //   return Container(width: 0.0, height: 0.0);
        // }

        // Once complete, show your application
        //if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            debugShowCheckedModeBanner: true,
            title: APP_NAME,
            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
                primaryColorBrightness: Brightness.dark,
                primaryColor: Color(0xFF25CCAA),
                accentColor: Color(0xFF73FADE),
                primaryIconTheme: IconThemeData(color: Colors.white),
                primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
                scaffoldBackgroundColor: Color(0xFFF4FFF4)
            ),
            localizationsDelegates: [
              // ... app-specific localization delegate[s] here
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('en', 'AU'),
              const Locale('en', 'BZ'),
              const Locale('en', 'CA'),
              const Locale('en', 'CB'),
              const Locale('en', 'GB'),
              const Locale('en', 'IE'),
              const Locale('en', 'JM'),
              const Locale('en', 'NZ'),
              const Locale('en', 'PH'),
              const Locale('en', 'TT'),
              const Locale('en', 'US'),
              const Locale('en', 'ZA'),
              const Locale('en', 'ZW'),
              const Locale('de', 'AT'),
              const Locale('de', 'CH'),
              const Locale('de', 'DE'),
              const Locale('de', 'LI'),
              const Locale('de', 'LU'),
              const Locale('sp', 'AR'),
              const Locale('sp', 'ES'),
              const Locale('sp', 'PR'),
              const Locale('sp', 'MX'),
              // Todo: More ESP
              const Locale('fr', 'BE'),
              const Locale('fr', 'CA'),
              const Locale('fr', 'CH'),
              const Locale('fr', 'FR'),
              const Locale('fr', 'LU'),
              const Locale('fr', 'MC'),
              const Locale('nl', 'BE'),
              const Locale('nl', 'NL'),
              const Locale('ja', 'JP'),
              const Locale('ko', 'KR'),
            ],
            home: widget,
          );
        //}

        // Otherwise, show something whilst waiting for initialization to complete
        //return Container(width: 0.0, height: 0.0);
      //},
    //);
   }
}
