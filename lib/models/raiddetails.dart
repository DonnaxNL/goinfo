import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class RaidDetails {
  bool iscurrent;
  String name;
  String image;
  String spritenormal;
  String spriteshiny;
  String cpnormal;
  String cpweather;
  String boostedby;

  // Reference to this record as a firestore document.
  final DocumentReference firestoreDocReference;

  RaidDetails.fromMap(Map<String, dynamic> map, {@required this.firestoreDocReference}) {
    assert(map['is-current'] != null);
    assert(map['name'] != null);
    assert(map['image'] != null);

    iscurrent = map['is-current'];
    name = map['name'];
    image = map['image'];
    spritenormal = map['sprite-normal'];
    spriteshiny = map['sprite-shiny'];
    cpnormal = map['cp-normal'];
    cpweather = map['cp-weather'];
    boostedby = map['boosted-by'];
  }

  RaidDetails.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), firestoreDocReference: snapshot.reference);
}