import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goinfo/models/pokemon.dart';
import 'package:goinfo/models/raiddetails.dart';
import 'package:goinfo/models/researchdetails.dart';

class RaidTab extends StatefulWidget {
  RaidTab({Key key}) : super(key: key);

  @override
  _RaidState createState() => _RaidState();
}

class _RaidState extends State<RaidTab> {
  Stream<QuerySnapshot> raidStream;

  @override
  void initState() {
    super.initState();
    raidStream = FirebaseFirestore.instance
        .collection('raids')
        .where("is-current", isEqualTo: true)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: StreamBuilder<QuerySnapshot>(
                stream: raidStream,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return CircularProgressIndicator();
                  } else {
                    final RaidDetails raidDetail = snapshot.data.docs
                        .map((snapshot) => RaidDetails.fromSnapshot(snapshot))
                        .toList()
                        .first;
                    return Center(
                        child: Column(children: <Widget>[
                      Container(
                          color: Theme.of(context).primaryColor,
                          width: double.infinity,
                          child: Image(image: NetworkImage(raidDetail.image))),
                      Container(
                          padding: const EdgeInsets.all(12.0),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0))),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(raidDetail.name,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18)),
                              SizedBox(height: 12.0),
                              Row(children: <Widget>[
                                Text("Catch CP:",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                                SizedBox(width: 8.0),
                                Text(raidDetail.cpnormal + " CP",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                              ]),
                              SizedBox(height: 6.0),
                              Row(children: <Widget>[
                                Text("Boosted CP:",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                                SizedBox(width: 8.0),
                                Text(raidDetail.cpweather + " CP",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                              ]),
                              SizedBox(height: 6.0),
                              Row(children: <Widget>[
                                Text("Boosted by:",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                                SizedBox(width: 8.0),
                                Text(raidDetail.boostedby,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18)),
                              ]),
                            ],
                          )),
                          SizedBox(height: 12.0),
                          Container(
                          child:
                              Column(mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                          Image(image: NetworkImage(raidDetail.spritenormal)),
                          Image(image: NetworkImage(raidDetail.spriteshiny)),
                        ]),
                        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                          Text("Normal",
                              style:
                                  TextStyle(fontSize: 18)),
                          Text("Shiny",
                              style:
                                  TextStyle(fontSize: 18)),
                        ]),
                      ]))
//                      GridView.count(
//                          crossAxisCount: 2,
//                          children: <Widget>[
//                            Image(image: NetworkImage(raidDetail.spritenormal)),
//                            Image(image: NetworkImage(raidDetail.spriteshiny)),
//                            Text("Normal"),
//                            Text("Shiny")
//                        ]
//                      )
                    ]));
                  }
                })));
  }

  // Build a list item corresponding to a research.
  Widget _buildListItem(ResearchDetails research) {
    return ExpansionTile(
      key: PageStorageKey<ResearchDetails>(research),
      title: Text(research.quest),
      children: _buildList(research),
    );
  }

  List<Widget> _buildList(ResearchDetails research) {
    List<Widget> list = new List();
//    for (Pokemon item in research.rewards) {
//      list.add(_buildPokemonItem(item));
//    }
    return list;
  }

  // Build a list item corresponding to a research.
  Widget _buildPokemonItem(Pokemon pokemon) {
    return ListTile(
      leading: Image.network(pokemon.mImageNormal),
      title: Text(pokemon.mName),
      subtitle: Text("cp " + pokemon.mResearchCp),
      trailing: Visibility(
          visible: pokemon.isShiny,
          child: Image.network(
            "https://leekduck.com/assets/img/shiny_icon.png",
            width: 18.0,
            height: 18.0,
          )),
    );
  }
}
